package wilk.wifi.pwd.ui;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;
import wilk.wifi.pwd.R;
import wilk.wifi.pwd.adapter.WiFiListAdapter;
import wilk.wifi.pwd.bean.WiFiBean;
import wilk.wifi.pwd.constant.BaseConstant;
import wilk.wifi.pwd.constant.PreferencesKey;
import wilk.wifi.pwd.effect.BlurBehind;
import wilk.wifi.pwd.effect.SlideInOutBottomItemAnimator;
import wilk.wifi.pwd.utils.LogUtils;
import wilk.wifi.pwd.utils.PreferencesUtils;
import wilk.wifi.pwd.utils.ShellUtils;
import wilk.wifi.pwd.utils.ToastUtils;
import wilk.wifi.pwd.utils.WifiManage;

/**
 * 首页界面
 *
 * @author Wilk
 */
public class MainActivity extends ActionBarActivity implements WiFiListAdapter.ActionClickListener, View.OnClickListener, PullRefreshLayout.OnRefreshListener {
    /**
     * 弹窗提示框
     */
    private MaterialDialog mMaterialDialog;
    /**
     * 列表
     */
    private RecyclerView mRecyclerView;
    /**
     * 下拉刷新控件
     */
    private PullRefreshLayout mPullRefreshLayout;
    /**
     * 工具栏
     */
    private Toolbar mToolbar;
    /**
     * FAB MENU
     */
    private FloatingActionsMenu mFABMenu;
    /**
     * FAB ABOUT
     */
    private FloatingActionButton mFABAbout;
    /**
     * FAB RELOAD
     */
    private FloatingActionButton mFABReload;
    /**
     * 列表adapter
     */
    private WiFiListAdapter mAdapter;
    /**
     * 解析出的WiFiBean列表
     */
    private ArrayList<WiFiBean> mWiFiBeanList = new ArrayList<>();
    /**
     * wifi管理器
     */
    private WifiManager mWifiManager;
    /**
     * 配置过的wifi列表
     */
    List<WifiConfiguration> mWifiConfigList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();
        bindEvent();
    }

    /**
     * 初始化view
     */
    private void initView() {
        mToolbar = (Toolbar) this.findViewById(R.id.toolbar);
        // 使用toolbar替代actionbar
        setSupportActionBar(mToolbar);

        mPullRefreshLayout = (PullRefreshLayout) this.findViewById(R.id.pulllayout);
        mPullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_MATERIAL);

        mRecyclerView = (RecyclerView) this.findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new SlideInOutBottomItemAnimator(mRecyclerView));

        mFABMenu = (FloatingActionsMenu) this.findViewById(R.id.fab_menu);
        mFABReload = (FloatingActionButton) this.findViewById(R.id.fab_reload);
        mFABAbout = (FloatingActionButton) this.findViewById(R.id.fab_about);
    }

    /**
     * 绑定事件
     */
    private void bindEvent() {
        mAdapter = new WiFiListAdapter(MainActivity.this, mWiFiBeanList, this);
        mRecyclerView.setAdapter(mAdapter);

        mPullRefreshLayout.setOnRefreshListener(this);

        mFABReload.setOnClickListener(this);
        mFABAbout.setOnClickListener(this);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        if (!PreferencesUtils.getBoolean(this, PreferencesKey.ALREADY_ENTER_APP, false)) {
            mMaterialDialog = new MaterialDialog(this)
                    .setTitle(R.string.tips)
                    .setMessage(R.string.description)
                    .setPositiveButton(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PreferencesUtils.putBoolean(getApplicationContext(), PreferencesKey.ALREADY_ENTER_APP, true);
                            mMaterialDialog.dismiss();
                        }
                    });
            mMaterialDialog.show();
        }

        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        // 获取root权限
        if (ShellUtils.checkRootPermission()) {
            readData();
        } else {
            ToastUtils.show(this, R.string.none_root, Toast.LENGTH_SHORT);
        }
    }

    /**
     * 读取数据
     */
    private void readData() {
        try {
            mWiFiBeanList = WifiManage.Read();
            if (BaseConstant.DEBUG) {
                for (int i = 0; i < mWiFiBeanList.size(); i++) {
                    LogUtils.d(MainActivity.class.getSimpleName(), mWiFiBeanList.get(i).toString());
                }
            }
        } catch (Exception exception) {
            ToastUtils.show(this, R.string.read_error, Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onActionClick(final int position, BaseConstant.ACTION_TYPE type) {
        if (mFABMenu.isExpanded()) {
            LogUtils.d("WILK", "onActionClick===>收缩FAB");
            mFABMenu.collapse();
        }

        if (type == BaseConstant.ACTION_TYPE.TRASH) {
            // 点击删除
            mWifiConfigList = mWifiManager.getConfiguredNetworks();
            String beanSsid = mWiFiBeanList.get(position).getSsid();
            for (int i = 0; i < mWifiConfigList.size(); i++) {
                String temp = mWifiConfigList.get(i).SSID;
                String wifiSsid = temp.substring(1, temp.length() - 1);
                LogUtils.d("WILK", wifiSsid + "/" + beanSsid);
                if (mWiFiBeanList.get(position).isHasChinese()) {
                } else if (wifiSsid.equals(beanSsid)) {
                    LogUtils.d("WILK", "删除" + beanSsid);
                    final int finalI = i;
                    mMaterialDialog = new MaterialDialog(this)
                            .setTitle(R.string.tips)
                            .setMessage(String.format(getString(R.string.delete_conform),beanSsid))
                            .setNegativeButton(R.string.delete, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();
                                    mWifiManager.disableNetwork(mWifiConfigList.get(finalI).networkId);
                                    mWifiManager.removeNetwork(mWifiConfigList.get(finalI).networkId);
                                    mWifiManager.saveConfiguration();
                                    mRecyclerView.getAdapter().notifyItemRemoved(position);
                                    mWiFiBeanList.remove(position);
                                    mRecyclerView.getAdapter().notifyItemRangeChanged(position, mRecyclerView.getAdapter().getItemCount());
                                }
                            }).setPositiveButton(R.string.cancel,new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mMaterialDialog.dismiss();
                                }
                            });
                    mMaterialDialog.show();
                    return;
                }
            }
        } else if (type == BaseConstant.ACTION_TYPE.SHARE) {
            // 点击分享
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(MainActivity.this, QRActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra(BaseConstant.INTENT_EXTRA_KEY_BEAN, mWiFiBeanList.get(position));
                    startActivity(intent);
                    overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);
                }
            };
            BlurBehind.getInstance().execute(MainActivity.this, runnable);
        } else if (type == BaseConstant.ACTION_TYPE.COPY) {
            // 点击复制
            ClipboardManager cmb = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            cmb.setText(mWiFiBeanList.get(position).getPwd());
            ToastUtils.show(getApplicationContext(), getString(R.string.copy_done));
        }
    }

    @Override
    public boolean onActionTouch(int position, BaseConstant.ACTION_TYPE type, boolean isOpened) {
        if (mFABMenu.isExpanded()) {
            LogUtils.d("WILK", "onActionTouch===>收缩FAB");
            mFABMenu.collapse();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (mFABMenu.isExpanded()) {
            LogUtils.d("WILK", "onClick===>收缩FAB");
            mFABMenu.collapse();
        }
        if (v == mFABAbout) {
            // 显示关于,鸣谢等...
            mMaterialDialog = new MaterialDialog(this)
                    .setTitle(R.string.fab_about)
                    .setMessage(R.string.about_info)
                    .setPositiveButton(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });
            mMaterialDialog.show();
        } else if (v == mFABReload) {
            // 刷新数据
            doRefresh();
        }
    }

    @Override
    public void onRefresh() {
        doRefresh();
    }

    /** 执行刷新数据的操作 */
    private void doRefresh() {
        readData();
        mRecyclerView.getAdapter().notifyItemRangeChanged(0, mRecyclerView.getAdapter().getItemCount());
        mRecyclerView.getAdapter().notifyDataSetChanged();
        mRecyclerView.smoothScrollToPosition(0);
        mPullRefreshLayout.setRefreshing(false);
    }
}