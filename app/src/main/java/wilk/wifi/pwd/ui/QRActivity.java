package wilk.wifi.pwd.ui;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.zxing.WriterException;

import java.io.File;
import java.io.IOException;

import wilk.wifi.pwd.R;
import wilk.wifi.pwd.bean.WiFiBean;
import wilk.wifi.pwd.constant.BaseConstant;
import wilk.wifi.pwd.effect.BlurBehind;
import wilk.wifi.pwd.utils.LogUtils;
import wilk.wifi.pwd.utils.QRUtils;
import wilk.wifi.pwd.utils.ToastUtils;

/**
 * 分享界面
 * @author Wilk
 */
public class QRActivity extends ActionBarActivity implements View.OnClickListener {
    /**
     * 当前分享的对象
     */
    private WiFiBean mBean;
    /**
     * 二维码显示控件
     */
    private ImageView mQRView;
    /**
     * 分享按钮
     */
    private BootstrapButton mShareBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        initView();
        initData();
        bindEvent();
    }

    /**
     * 初始化view
     */
    private void initView() {
        BlurBehind.getInstance().setBackground(this);
        mQRView = (ImageView) this.findViewById(R.id.qr_image);
        mShareBtn = (BootstrapButton) this.findViewById(R.id.share_btn);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        mBean = (WiFiBean) getIntent().getSerializableExtra(BaseConstant.INTENT_EXTRA_KEY_BEAN);
        try {
            LogUtils.d("WILK", "QR.DATA===>" + QRUtils.generateQRDataString(mBean));
            QRUtils.generateQRCode(QRUtils.generateQRDataString(mBean), mQRView, BitmapFactory.decodeResource(getResources(), R.drawable.logo));
        } catch (WriterException exception) {
            ToastUtils.show(QRActivity.this, getString(R.string.qr_gen_failed));
            finish();
        }
    }

    /**
     * 绑定事件
     */
    private void bindEvent() {
        mShareBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mShareBtn)
            shareMsg();
    }

    /**
     * 分享数据
     */
    public void shareMsg() {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            File file = new File(QRUtils.saveImageToGallery(((BitmapDrawable) mQRView.getDrawable()).getBitmap()));
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(Intent.createChooser(intent, "分享"));
        } catch (IOException ex) {
            ToastUtils.show(QRActivity.this, getString(R.string.qr_share_failed));
        }
    }
}
