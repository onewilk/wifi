package wilk.wifi.pwd.bean;

import java.io.Serializable;

/**
 * Created by Wilk on 2014/12/8.
 */
public class WiFiBean implements Serializable {
    private String ssid;
    private String pwd;
    private String type;
    private String priority;
    private boolean hasChinese;

    public WiFiBean() {
    }

    public WiFiBean(String ssid, String pwd, String type, String priority, boolean hasChinese) {
        this.ssid = ssid;
        this.pwd = pwd;
        this.type = type;
        this.priority = priority;
        this.hasChinese = hasChinese;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public boolean isHasChinese() {
        return hasChinese;
    }

    public void setHasChinese(boolean hasChinese) {
        this.hasChinese = hasChinese;
    }

    @Override
    public String toString() {
        return "ssid/pwd/type/priority/hasChinese===>" + ssid + "/" + pwd + "/" + type + "/" + priority + "/" + hasChinese;
    }

}
