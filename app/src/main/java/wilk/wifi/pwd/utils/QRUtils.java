package wilk.wifi.pwd.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;

import wilk.wifi.pwd.bean.WiFiBean;
import wilk.wifi.pwd.constant.BaseConstant;

/**
 * 二维码工具类
 * Created by Mr.Wilk on 2015/01/17 0017.
 */
public class QRUtils {
    /**
     * 二维码尺寸
     */
    private static int DEFAULT_WIDTH_HEIGHT = 600;

    /**
     * 根据对象生成符合二维码WIFI数据格式的data字符串
     *
     * @param bean WiFiBean
     * @return 符合二维码WIFI数据格式的data字符串
     */
    public static String generateQRDataString(WiFiBean bean) {
        StringBuffer buffer = new StringBuffer("WIFI:");
        if (bean.getType().toLowerCase().contains("wpa")) {
            buffer.append("T:WPA;");
        } else if (bean.getType().toLowerCase().contains("wep")) {
            buffer.append("T:WEP;");
        }
        buffer.append("S:" + bean.getSsid() + ";");
        if (bean.getPwd().equals(BaseConstant.NULL_VALUE))
            buffer.append("P:;");
        else
            buffer.append("P:" + bean.getPwd() + ";");
        buffer.append(";");
        return buffer.toString();
    }

    /**
     * 生成二维码并显示到指定的ImageView上
     *
     * @param data    符合二维码WIFI数据格式的data字符串
     * @param img     最终要显示的ImageView
     * @param logoBmp 添加的logo图片,大小建议不要超过60px,否则影响二维码最终的识别率
     * @throws WriterException 生成二维码失败时的异常
     * @see QRUtils#generateQRDataString(wilk.wifi.pwd.bean.WiFiBean)
     */
    public static void generateQRCode(String data, ImageView img, Bitmap logoBmp) throws WriterException {
        Bitmap qrCodeBitmap = createQRCode(data, DEFAULT_WIDTH_HEIGHT);
        Bitmap bitmap = Bitmap.createBitmap(qrCodeBitmap.getWidth(), qrCodeBitmap
                .getHeight(), qrCodeBitmap.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(qrCodeBitmap, 0, 0, null);
        canvas.drawBitmap(logoBmp, qrCodeBitmap.getWidth() / 2
                - logoBmp.getWidth() / 2, qrCodeBitmap.getHeight()
                / 2 - logoBmp.getHeight() / 2, null);
        img.setImageBitmap(bitmap);
    }

    /**
     * 生成二维码,返回bitmap
     * @param str 符合二维码WIFI数据格式的data字符串
     * @param widthAndHeight
     * @return 二维码的bitmap
     * @throws WriterException 生成二维码失败时的异常
     */
    public static Bitmap createQRCode(String str, int widthAndHeight) throws WriterException {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix matrix = new MultiFormatWriter().encode(str,
                BarcodeFormat.QR_CODE, widthAndHeight, widthAndHeight, hints);
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (matrix.get(x, y)) {
                    // 二维码内容区颜色
                    pixels[y * width + x] = Color.BLACK;
                } else {
                    // 二维码非内容区颜色
                    pixels[y * width + x] = Color.WHITE;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    /**
     * 保存bitmap到文件,安全起见没有使用图片格式的后缀名
     * @param bmp 要保存的bitmap
     * @return 保存后的路径
     * @throws IOException io异常
     */
    public static String saveImageToGallery(Bitmap bmp) throws IOException {
        File dir = new File(BaseConstant.FILE_SAVE_PATH);
        if (!dir.exists())
            dir.mkdir();
        File file = new File(BaseConstant.FILE_SAVE_PATH, "temp");
        if (file.exists())
            file.delete();
        FileOutputStream fos = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.flush();
        fos.close();
        return file.getAbsolutePath();
    }
}