package wilk.wifi.pwd.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import wilk.wifi.pwd.bean.WiFiBean;
import wilk.wifi.pwd.constant.BaseConstant;

/**
 * wifi管理工具类
 * Created by Wilk on 2015/1/7.
 */
public class WifiManage {
    private static ArrayList<WiFiBean> wifiBeans = new ArrayList<>();

    /**
     * 读取wifi列表
     *
     * @return wifi列表
     * @throws Exception
     */
    public static ArrayList<WiFiBean> Read() throws Exception {
        wifiBeans.clear();
        Process process = null;
        DataOutputStream dataOutputStream = null;
        DataInputStream dataInputStream = null;
        StringBuffer wifiConf = new StringBuffer();
        process = Runtime.getRuntime().exec("su");
        dataOutputStream = new DataOutputStream(process.getOutputStream());
        dataInputStream = new DataInputStream(process.getInputStream());
        dataOutputStream
                .writeBytes("cat /data/misc/wifi/*.conf\n");
        dataOutputStream.writeBytes("exit\n");
        dataOutputStream.flush();
        InputStreamReader inputStreamReader = new InputStreamReader(
                dataInputStream, "UTF-8");
        BufferedReader bufferedReader = new BufferedReader(
                inputStreamReader);
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            wifiConf.append(line);
        }
        bufferedReader.close();
        inputStreamReader.close();
        process.waitFor();
        try {
            if (dataOutputStream != null) {
                dataOutputStream.close();
            }
            if (dataInputStream != null) {
                dataInputStream.close();
            }
            process.destroy();
        } catch (Exception e) {
            throw e;
        }

        Pattern network = Pattern.compile("network=\\{([^\\}]+)\\}", Pattern.DOTALL);
        Matcher networkMatcher = network.matcher(wifiConf.toString());
        while (networkMatcher.find()) {
            String networkBlock = networkMatcher.group();
            LogUtils.d(WifiManage.class.getSimpleName(), networkBlock);

            Pattern ssid = Pattern.compile("ssid=[^\\s]+");
            Matcher ssidMatcher = ssid.matcher(networkBlock);

            if (ssidMatcher.find()) {
                WiFiBean bean = new WiFiBean();
                String result = ssidMatcher.group().split("=")[1];
                if (result.startsWith("\"") && result.endsWith("\"") && result.length() > 2) {
                    bean.setHasChinese(false);
                } else if (!result.startsWith("\"") && !result.endsWith("\"")) {
                    bean.setHasChinese(true);
                }
                bean.setSsid(getFixString(ssidMatcher, true));
                Pattern psk = Pattern.compile("psk=[^\\s]+");
                Matcher pskMatcher = psk.matcher(networkBlock);
                bean.setPwd(getFixString(pskMatcher, false));

                Pattern type = Pattern.compile("key_mgmt=[^\\s]+");
                Matcher typeMatcher = type.matcher(networkBlock);
                bean.setType(getFixString(typeMatcher, false));

                Pattern priority = Pattern.compile("priority=[^\\s]+");
                Matcher priorityMatcher = priority.matcher(networkBlock);
                bean.setPriority(getFixString(priorityMatcher, false));

                wifiBeans.add(bean);
            }
        }
        return wifiBeans;
    }

    /**
     * 移除前后的双引号和其他多余的符号
     *
     * @param matcher
     * @param force   强制移除
     * @return
     */
    public static String getFixString(Matcher matcher, boolean force) {
        String result;
        if (force || matcher.find()) {
            result = matcher.group().split("=")[1];
            if (result.startsWith("\"") && result.endsWith("\"") && result.length() > 2) {
                result = result.substring(1, result.length() - 1);
            }
            if (result.endsWith("}")) {
                result = result.substring(0, result.length() - 1);
            }
        } else {
            result = BaseConstant.NULL_VALUE;
        }
        return result;
    }
}