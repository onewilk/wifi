package wilk.wifi.pwd.constant;

import android.os.Environment;

/**
 * Created by Wilk on 2014/12/8.
 */
public class BaseConstant {
    public static boolean DEBUG = true;

    public static String NULL_VALUE = "-1";

    public static String INTENT_EXTRA_KEY_BEAN = "BEAN";

    public static String FILE_SAVE_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath()+"/WIFIPWD/";

    public enum ACTION_TYPE {
        TRASH, SHARE, COPY,HEADER,CONTENT
    }
}
