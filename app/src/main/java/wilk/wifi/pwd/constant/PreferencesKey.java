package wilk.wifi.pwd.constant;

/**
 * PreferencesConstant
 *
 * @version 1.0.0
 */
public class PreferencesKey {
    /**
     * 标识是否首次进入应用
     */
    public static final String ALREADY_ENTER_APP = "ALREADY_ENTER_APP";
}