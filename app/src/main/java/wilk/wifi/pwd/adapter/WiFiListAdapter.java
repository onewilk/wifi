package wilk.wifi.pwd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;

import java.util.ArrayList;

import wilk.wifi.pwd.R;
import wilk.wifi.pwd.bean.WiFiBean;
import wilk.wifi.pwd.constant.BaseConstant;

/**
 * Wifi列表的adapter
 * Created by Wilk on 2015/1/15.
 */
public class WiFiListAdapter extends RecyclerView.Adapter<WiFiListAdapter.ViewHolder> implements View.OnClickListener, View.OnTouchListener {
    private Context mContext;
    /**
     * wifi列表
     */
    private ArrayList<WiFiBean> mWifiList = new ArrayList<>();
    /**
     * 按钮点击事件
     */
    private ActionClickListener mActionClickListener;

    /**
     * 按钮点击事件
     */
    public interface ActionClickListener {
        /**
         * 按钮点击事件
         *
         * @param position 点击的当前位置
         * @param type     按钮事件类型
         * @see wilk.wifi.pwd.constant.BaseConstant.ACTION_TYPE
         */
        public void onActionClick(int position, BaseConstant.ACTION_TYPE type);

        /**
         * 按钮触摸事件
         *
         * @param type 按钮事件类型
         * @return 是否拦截touch
         * @see wilk.wifi.pwd.constant.BaseConstant.ACTION_TYPE
         */
        public boolean onActionTouch(int position,BaseConstant.ACTION_TYPE type,boolean isOpened);
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        int id = v.getId();
        if (mActionClickListener != null) {
            if (id == R.id.trash_ll) {
                // 删除
                mActionClickListener.onActionClick(position, BaseConstant.ACTION_TYPE.TRASH);
            } else if (id == R.id.share_ll) {
                // 分享
                mActionClickListener.onActionClick(position, BaseConstant.ACTION_TYPE.SHARE);
            } else if (id == R.id.copy_ll) {
                // 复制
                mActionClickListener.onActionClick(position, BaseConstant.ACTION_TYPE.COPY);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return mActionClickListener.onActionTouch((int) v.getTag(R.id.position),(BaseConstant.ACTION_TYPE) v.getTag(R.id.type),((boolean)v.getTag(R.id.opened)));
    }

    /**
     * 　视图控制器
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        /**
         * wifi名
         */
        TextView ssid;
        /**
         * 密码
         */
        TextView pwd;
        /**
         * 加密类型
         */
        TextView type;
        /**
         * 优先级
         */
        TextView yxj;
        /**
         * 删除按钮
         */
        LinearLayout trashll;
        /**
         * 分享按钮
         */
        LinearLayout sharell;
        /**
         * 复制按钮
         */
        LinearLayout copyll;
        /** 可展开的容器 */
        ExpandableLayout mExpandLayout;
        /**
         * 未展开的内容
         */
        RelativeLayout header;
        /**
         * 展开后的内容
         */
        RelativeLayout content;

        public ViewHolder(View v) {
            super(v);
            mExpandLayout = (ExpandableLayout) v.findViewById(R.id.expand);
            header = mExpandLayout.getHeaderRelativeLayout();
            content = mExpandLayout.getContentRelativeLayout();

            this.ssid = (TextView) header.findViewById(R.id.ssid);
            this.pwd = (TextView) header.findViewById(R.id.pwd);
            this.type = (TextView) header.findViewById(R.id.type);
            this.yxj = (TextView) header.findViewById(R.id.yxj);
            this.trashll = (LinearLayout) content.findViewById(R.id.trash_ll);
            this.sharell = (LinearLayout) content.findViewById(R.id.share_ll);
            this.copyll = (LinearLayout) content.findViewById(R.id.copy_ll);
        }
    }

    /**
     * adapter实例化方法
     */
    public WiFiListAdapter(Context context, ArrayList<WiFiBean> wiFiBeans, ActionClickListener listener) {
        this.mContext = context;
        this.mWifiList = wiFiBeans;
        this.mActionClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mExpandLayout.hide();
        WiFiBean bean = mWifiList.get(position);
        holder.ssid.setText(String.format(mContext.getString(R.string.ssid), bean.getSsid()));
        if (bean.getPwd().equals(BaseConstant.NULL_VALUE)) {
            holder.pwd.setText(String.format(mContext.getString(R.string.pwd), mContext.getString(R.string.no_pwd)));
        } else {
            holder.pwd.setText(String.format(mContext.getString(R.string.pwd), bean.getPwd()));
        }
        holder.type.setText(String.format(mContext.getString(R.string.type), bean.getType()));
        if (bean.getPriority().equals(BaseConstant.NULL_VALUE)) {
            holder.yxj.setText(String.format(mContext.getString(R.string.yxj), mContext.getString(R.string.no_yxj)));
        } else {
            holder.yxj.setText(String.format(mContext.getString(R.string.yxj), bean.getPriority()));
        }
        holder.trashll.setTag(position);
        holder.copyll.setTag(position);
        holder.sharell.setTag(position);

        holder.header.setTag(R.id.position, position);
        holder.header.setTag(R.id.opened, holder.mExpandLayout.isOpened());
        holder.header.setTag(R.id.type,BaseConstant.ACTION_TYPE.HEADER);
        holder.content.setTag(R.id.position,position);
        holder.content.setTag(R.id.opened, holder.mExpandLayout.isOpened());
        holder.content.setTag(R.id.type,BaseConstant.ACTION_TYPE.CONTENT);

        holder.trashll.setOnClickListener(this);
        holder.copyll.setOnClickListener(this);
        holder.sharell.setOnClickListener(this);

        holder.header.setOnTouchListener(this);
        holder.content.setOnTouchListener(this);
    }

    @Override
    public int getItemCount() {
        return mWifiList.size();
    }
}